import XCTest

import TuneSearchTests

var tests = [XCTestCaseEntry]()
tests += TuneSearchTests.allTests()
XCTMain(tests)
