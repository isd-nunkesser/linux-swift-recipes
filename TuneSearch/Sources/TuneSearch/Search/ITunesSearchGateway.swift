//
//  ITunesSearchGateway.swift
//  TuneSearch
//
//  Created by Prof. Dr. Nunkesser, Robin on 19.12.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

enum HTTPResponseError: Error {
    case serverError
}

extension HTTPResponseError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .serverError:
            return "HTTP Error"
        }
    }
}


class ITunesSearchGateway {
    let baseURLString = "https://itunes.apple.com/search"
    let session = URLSession(configuration: .default)
    
    func restURL(_ parameters: [String:String]?) -> URL? {
        var components = URLComponents(string: baseURLString)!
        var queryItems = [URLQueryItem]()
        if let additionalParams = parameters {
            for (key,value) in additionalParams {
                let item = URLQueryItem(name: key, value: value)
                queryItems.append(item)
            }
        }
        components.queryItems = queryItems
        return components.url!
    }
    
    func fetchData(searchTerm: String, completion: @escaping (Swift.Result<[TrackEntity],Error>) -> Void) {        
        if let url = restURL(["term":"\(searchTerm)","entity":"song","country":"de"]) {
            debugPrint(url)
            let task = session.dataTask(with: url) {
                (data, response, error) -> Void in
                if let error = error {
                    completion(Result.failure(error))
                    return
                }
                guard let httpResponse = response as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode),
                    let data = data else {
                    completion(Result.failure(HTTPResponseError.serverError))
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let items = try decoder.decode(ResultsEntity.self,
                                                   from: data)
                    completion(.success(items.results))
                } catch  {
                    completion(.failure(error))
                }
            }
            task.resume()
        }
    }
}
