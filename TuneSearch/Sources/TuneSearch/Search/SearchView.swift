//
//  ContentView.swift
//  TuneSearch
//
//  Created by Prof. Dr. Nunkesser, Robin on 15.01.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftWebUI
import BasicCleanArch

struct SearchView: View, Displayer {
    
    typealias ViewModelType = [CollectionViewModel]
    @State var searchText = "Jack Johnson"
    
    var tracksListView = TracksListView()
    
    var body: some View {
        NavigationView {
            VStack(alignment: .center) {
                TextField($searchText, placeholder: Text("Searchterm"))
                Button(action: {self.startSearch()}, label: Text("Search"))
                NavigationLink(destination: tracksListView) {
                    Text("Show")
                }
            }
            .padding(.horizontal)
            .navigationBarTitle(Text("Search"))
        }
    }
    
    func startSearch() {        
        SearchInteractor(presenter: CollectionsPresenter()).execute(request: SearchRequest(term: searchText), displayer: self)
    }
 
    func display(failure: Error) {
        debugPrint(failure.localizedDescription)
    }
    
    func display(success: [CollectionViewModel], resultCode: Int) {
        tracksListView.viewModel.collections = success
    }

}

