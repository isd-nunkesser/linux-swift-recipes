import SwiftWebUI
import Foundation

struct TrackRow: View {
    var text : String
    var detailText: String
    var url: URL
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(verbatim: text)
                    .font(.subheadline)
                Text(verbatim: detailText)
                    .font(.caption)
            }
        }
    }
}

