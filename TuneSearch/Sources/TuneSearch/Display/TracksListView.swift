//
//  TracksListView.swift
//  TuneSearch
//
//  Created by Prof. Dr. Nunkesser, Robin on 15.01.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftWebUI
import BasicCleanArch
import Foundation

struct CollectionViewModel : Identifiable {
    let id = UUID()
    var name: String
    var tracks: [TrackViewModel]
}

struct TrackViewModel : Identifiable {
    let id = UUID()
    var title: String
    var subtitle: String
    let image: URL

}

struct TracksListView: View {

    var viewModel = CollectionsViewModel()
        
    var body: some View {
        List {
            ForEach(viewModel.collections) { collection in
                Section(header: Text(collection.name)) {
                    ForEach(collection.tracks) {
                        TrackRow(text: $0.title, detailText: $0.subtitle, url: $0.image)
                    }
                }
            }
        }.navigationBarTitle(Text("Collections"))
    }
    
}

