import XCTest

import BasicInteractionTests

var tests = [XCTestCaseEntry]()
tests += BasicInteractionTests.allTests()
XCTMain(tests)
