import SwiftWebUI

struct ContentView: View {
    @State var text = ""
    @State var outputText = ""
    
    var body: some View {
        VStack(alignment: .leading) {
            TextField($text, placeholder: Text("Enter text"))
            Button(action: { self.process() }) {
                Text("Process")
            }
            Text(outputText)
        }
    }
    
    func process() {
        outputText = text.uppercased()
    }
}
