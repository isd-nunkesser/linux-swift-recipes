import SwiftWebUI

struct StatisticsView: View {
    @State var viewModel : ContentViewModel
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                HStack {
                    Text("Bearbeitete Fragen")
                    Spacer()
                    Text("\(viewModel.questionsSeen)")
                }
                HStack {
                    Text("Richtig beantwortet")
                    Spacer()
                    Text("\(viewModel.correctAnswers)")
                }
                HStack {
                    Text("Falsch beantwortet")
                    Spacer()
                    Text("\(viewModel.wrongAnswers)")
                }
                HStack {
                    Text("Übersprungen")
                    Spacer()
                    Text("\(viewModel.skippedQuestions)")
                }
                Spacer()
            }
            .padding(.all)
            .navigationBarTitle(Text("Statistics"))
        }
    }
}

