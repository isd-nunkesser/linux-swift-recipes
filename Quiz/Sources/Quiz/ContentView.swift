//
//  ContentView.swift
//  Quiz
//
//  Created by Prof. Dr. Nunkesser, Robin on 19.12.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftWebUI

struct ContentView: View {
    @State var viewModel : ContentViewModel
    
    var body: some View {
        NavigationView {
            VStack() {
                NavigationLink(destination: StatisticsView(viewModel: viewModel)) {
                    Text("Statistics")
                }
                Spacer()
                Text(viewModel.question)
                    .font(.headline)
                Spacer()
                Text(viewModel.answer)
                    .color(viewModel.resultColor)
                Toggle(isOn: $viewModel.answerFalse) {
                    Text("Falsch")
                }.disabled(viewModel.buttonsDisabled)
                Toggle(isOn: $viewModel.answerTrue) {
                    Text("Richtig")
                }.disabled(viewModel.buttonsDisabled)
                Toggle(isOn: $viewModel.skip) {
                    Text("Überspringen")
                }.disabled(viewModel.buttonsDisabled)
            }
            .padding(.all)
            .navigationBarTitle(Text("Quiz"))
        }
    }
}

