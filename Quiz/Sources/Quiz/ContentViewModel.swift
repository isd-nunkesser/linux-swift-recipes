import SwiftWebUI
import Foundation

final class ContentViewModel {
    
    var questions = [
    ("Das Videospiel Donkey Kong sollte ursprünglich Popeye als Hauptfigur haben.", true),
    ("Die Farbe Orange wurde nach der Frucht benannt.", true),
    ("In der griechischen Mythologie ist Hera die Göttin der Ernte.", false),
    ("Liechtenstein hat keinen eigenen Flughafen.", true),
    ("Die meisten Subarus werden in China hergestellt.", false)]
    
    var index = 0 {
        didSet {
            question = questions[index].0
        }
    }    
    var question = ""
    var answer = "XXXXXXXXXX"
    var resultOpacity = 0.0
    var resultColor : Color {
        get { buttonsDisabled ? Color.black : Color.white }
    }
    var buttonsDisabled = false
    var skip = false {
        willSet {
            skipQuestion()            
        }
    }
    var answerFalse = false {
        willSet {
            evaluateAnswer(answer: false)
        }
    }
    var answerTrue = false {
        willSet {
            evaluateAnswer(answer: true)
        }
    }

    var correctAnswers = 0
    var wrongAnswers = 0
    var skippedQuestions = 0
    var questionsSeen: Int {
        return correctAnswers + wrongAnswers + skippedQuestions
    }
    
    init() {
        question = questions[index].0
    }
    
    func skipQuestion() {
        nextQuestion()
        skippedQuestions += 1
    }
    
    func nextQuestion() {
        index = (index + 1) % questions.count
        resultOpacity = 0.0
        buttonsDisabled = false
    }
    
    func evaluateAnswer(answer: Bool) {
        buttonsDisabled = true
        resultOpacity = 1.0
        if (questions[index].1 == answer) {
            self.answer = "Richtig!"
            correctAnswers += 1
        } else {
            self.answer = "Falsch!"
            wrongAnswers += 1
        }
        
        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: DispatchTime.now() + .milliseconds(10)) {
            self.nextQuestion()
        }

    }
}

