import XCTest

import QuizTests

var tests = [XCTestCaseEntry]()
tests += QuizTests.allTests()
XCTMain(tests)
