import XCTest

import ListRecipeTests

var tests = [XCTestCaseEntry]()
tests += ListRecipeTests.allTests()
XCTMain(tests)
