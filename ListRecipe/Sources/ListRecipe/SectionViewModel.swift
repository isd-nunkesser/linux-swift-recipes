import SwiftWebUI
import Foundation

struct SectionViewModel : Identifiable {
    let id = UUID()
    var header: LocalizedStringKey
    var footer: LocalizedStringKey?
    var items: [ItemViewModel]
}
