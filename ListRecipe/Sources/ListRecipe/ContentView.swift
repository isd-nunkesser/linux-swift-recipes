import SwiftWebUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List {
                NavigationLink(destination: SimpleListView()) {
                    Text("Nur Anzeige")
                }
                NavigationLink(destination: SectionListView()) {
                    Text("Nur Anzeige (mit Sections)")
                }
                NavigationLink(destination: AddDeleteListView()) {
                    Text("Hinzufügen und Löschen")
                }
            }
            .navigationBarTitle(Text("List Examples"))
        }
    }
}

