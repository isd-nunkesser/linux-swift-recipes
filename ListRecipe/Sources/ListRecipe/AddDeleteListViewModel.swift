import Foundation
import SwiftWebUI

final class AddDeleteListViewModel {
    var items = [ItemViewModel(text: "Dummy", detailText: "")]
    
    var add = false {
        willSet {
            addItem()
        }
    }

    var delete = false {
        willSet {
            deleteItem(indexSet: IndexSet(integer: 0))
        }
    }

    func addItem() {
        let newItem = ItemViewModel(text: "\(UUID().uuidString)",
        detailText: "\(UUID().uuidString)")
        items.append(newItem)
    }
    
    func deleteItem(indexSet: IndexSet) {
        // Only one deletion at a time allowed in this example
        guard let index = indexSet.first else { return }
        items.remove(at: index)
    }

}
