import Foundation
import SwiftWebUI

struct ItemViewModel : Identifiable {
    let id = UUID()
    var text: LocalizedStringKey
    var detailText: LocalizedStringKey
}
