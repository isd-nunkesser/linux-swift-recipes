import SwiftWebUI

struct AddDeleteListView: View {
    @State var viewModel = AddDeleteListViewModel()
    
    var body: some View {
        Group {
            HStack{
                Toggle(isOn: $viewModel.add) {
                    Text("Add")
                }
                Toggle(isOn: $viewModel.delete) {
                    Text("Delete")
                }                
            }
            List {
                ForEach(viewModel.items) {
                    SubtitleRow(item: $0)
                }
            }
        }
        .navigationBarTitle(Text("Add / Delete List View"))
    }
    
}

