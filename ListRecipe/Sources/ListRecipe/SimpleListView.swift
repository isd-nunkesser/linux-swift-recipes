import SwiftWebUI

struct SimpleListView: View {
    let items = [ItemViewModel(text: "Title 1", detailText: "Subtitle 1"),
                 ItemViewModel(text: "Title 2", detailText: "Subtitle 2")]
    
    var body: some View {        
        return List(items, rowContent: SubtitleRow.init)
        .navigationBarTitle(Text("Simple List View"))
    }
}

