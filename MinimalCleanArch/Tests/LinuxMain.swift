import XCTest

import MinimalCleanArchTests

var tests = [XCTestCaseEntry]()
tests += MinimalCleanArchTests.allTests()
XCTMain(tests)
