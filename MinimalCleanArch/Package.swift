// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MinimalCleanArch",
    platforms: [
        .macOS(.v10_15)
    ],
    dependencies: [
        .package(url: "https://github.com/SwiftWebUI/SwiftWebUI.git", from: "0.3.0"),
        .package(url: "https://github.com/RobinNunkesser/basiccleanarch-cocoapod.git", from: "5.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "MinimalCleanArch",
            dependencies: ["SwiftWebUI","BasicCleanArch"]),
        .testTarget(
            name: "MinimalCleanArchTests",
            dependencies: ["MinimalCleanArch"]),
    ]
)
