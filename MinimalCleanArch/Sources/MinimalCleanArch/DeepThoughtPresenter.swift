//
//  DeepThoughtPresenter.swift
//  DeepThought
//
//  Created by Prof. Dr. Nunkesser, Robin on 01.05.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class DeepThoughtPresenter : Presenter {
    typealias Model = Int
    typealias ViewModel = String

    func present(model: Int) -> String {
        return model.description
    }
}
