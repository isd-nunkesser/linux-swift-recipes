//
//  DeepThoughtInteractor.swift
//  DeepThought
//
//  Created by Prof. Dr. Nunkesser, Robin on 01.05.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class DeepThoughtInteractor : UseCase {
    typealias DisplayerType = ContentView
    typealias PresenterType = DeepThoughtPresenter
    typealias RequestType = Void?
    
    var presenter : PresenterType
    
    required init(presenter: PresenterType) {
        self.presenter = presenter
    }

    func execute(request: Void?, displayer: DisplayerType, resultCode: Int) {
        // Deep thought does a lot of work and the result is
        let entity = 42
        let viewModel = presenter.present(model: entity)
        displayer.display(success: viewModel, resultCode: resultCode)                
    }
    
}
