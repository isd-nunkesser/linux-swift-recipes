//
//  ContentView.swift
//  DeepThought
//
//  Created by Prof. Dr. Nunkesser, Robin on 01.05.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftWebUI
import BasicCleanArch

struct ContentView: View, Displayer {
    typealias ViewModelType = String
    
    let interactor = DeepThoughtInteractor(presenter: DeepThoughtPresenter())
    
    @State var result = ""
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: startInteraction) {
                Text("Start")
            }
            Text(result)
        }
    }
    
    func startInteraction() {
        interactor.execute(request: nil, displayer: self)
    }
    
    func display(success: String, resultCode: Int) {
        result = success
    }

    func display(failure: Error) {
        debugPrint(failure.localizedDescription)
    }

}

