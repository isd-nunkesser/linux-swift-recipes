import SwiftWebUI
import BasicCleanArch

struct ContentView: View, Displayer {
    typealias ViewModelType = String
    
    @State var result = "..."
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: startFoundationInteraction) {
                Text("Start")
            }
            Text(result)
        }
    }
    
    func startInteraction(gateway: HttpBinGateway) {
        let ia = GetHttpRequestInteractor(presenter: HttpRequestPresenter(),
                                          gateway: gateway)
        ia.execute(request: nil, displayer: self)
    }
    
    func startFoundationInteraction() {
        startInteraction(gateway: HttpBinFoundationGateway())
    }

    func display(success: String, resultCode: Int) {
        debugPrint(success)
        result = success
    }
    
    func display(failure: Error) {
        debugPrint(failure.localizedDescription)
    }
    
}
