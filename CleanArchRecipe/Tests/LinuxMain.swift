import XCTest

import CleanArchRecipeTests

var tests = [XCTestCaseEntry]()
tests += CleanArchRecipeTests.allTests()
XCTMain(tests)
