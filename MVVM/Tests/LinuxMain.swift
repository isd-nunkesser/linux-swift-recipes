import XCTest

import MVVMTests

var tests = [XCTestCaseEntry]()
tests += MVVMTests.allTests()
XCTMain(tests)
