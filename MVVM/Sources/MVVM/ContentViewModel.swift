import SwiftWebUI

final class ContentViewModel {
    var forename = "" {
        didSet {
            greeting = "Hello \(forename)!"
        }
    }
    var surname = ""
    var greeting = ""

}

