import SwiftWebUI

struct ContentView: View {
    @State var viewModel = ContentViewModel()
    
    var body: some View {
        VStack() {
            TextField($viewModel.forename, placeholder: Text("Forename"))
            TextField($viewModel.surname, placeholder: Text("Surname"))
            HStack {
                Text("Forename: ")
                Text(viewModel.forename)
            }
            HStack {
                Text("Surname: ")
                Text(viewModel.surname)
            }
            Text(viewModel.greeting)
        }
        .padding(.horizontal)
    }
}

