import Foundation
import OpenSwiftUI

extension Text: ViewBuildable {
    public func build() {
        switch _storage {
        case .verbatim(let content):
            debugPrint(content)
        case .anyTextStorage(let content):
            debugPrint(content.storage)            
        }
    }
}
