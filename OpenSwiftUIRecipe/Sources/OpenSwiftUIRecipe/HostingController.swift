import Foundation
import OpenSwiftUI

public class HostingController<Content: View> {
    private var rootView: Content
    
    public init(rootView: Content) {
        self.rootView = rootView        
        (rootView.body as? ViewBuildable)?.build()
    }
}
