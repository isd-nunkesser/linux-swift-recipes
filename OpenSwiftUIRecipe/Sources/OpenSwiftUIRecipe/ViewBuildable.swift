import OpenSwiftUI

public protocol ViewBuildable {
    func build()
}
