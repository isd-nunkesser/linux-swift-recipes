// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "OpenSwiftUIRecipe",
    platforms: [
        .macOS(.v10_15)
    ],
    dependencies: [
        .package(url: "https://github.com/RobinNunkesser/OpenSwiftUI.git", .branch("master")),
    ],
    targets: [
        .target(
            name: "OpenSwiftUIRecipe",
            dependencies: ["OpenSwiftUI"]),
        .testTarget(
            name: "OpenSwiftUIRecipeTests",
            dependencies: ["OpenSwiftUIRecipe"]),
    ]
)
