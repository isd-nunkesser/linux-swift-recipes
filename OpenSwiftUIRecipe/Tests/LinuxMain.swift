import XCTest

import OpenSwiftUIRecipeTests

var tests = [XCTestCaseEntry]()
tests += OpenSwiftUIRecipeTests.allTests()
XCTMain(tests)
