import SwiftWebUI
import Foundation

struct ContentView: View {
    @State var result = "..."
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: start) {
                Text("Start")
            }
            Text(result)
        }
    }
    
    func start() {
        asyncCall(completion: {
            switch $0 {
            case let .success(value):
                debugPrint(value)
                self.result = value
            case let .failure(error): debugPrint(error)
            }})
    }
    
    func asyncCall(completion: @escaping (Result<String,Error>) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            sleep(1)
            completion(.success("My return value"))
        }
    }
}

