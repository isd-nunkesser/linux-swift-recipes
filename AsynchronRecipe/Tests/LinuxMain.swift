import XCTest

import AsynchronRecipeTests

var tests = [XCTestCaseEntry]()
tests += AsynchronRecipeTests.allTests()
XCTMain(tests)
