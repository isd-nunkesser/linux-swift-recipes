import SwiftWebUI

struct SecondLevelView: View {
    var text : String
    
    var body: some View {
        Text(text)
            .navigationBarTitle(Text("Second level"))
    }
}
