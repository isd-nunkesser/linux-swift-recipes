import SwiftWebUI

struct FirstView: View {
    var body: some View {
        NavigationView(emptyView: SecondLevelView(text: "Detail")) {
            VStack {
                Text("Master")
                NavigationLink(destination: SecondLevelView(text: "Passed data")) {
                    Text("Navigate")
                }
            }
            .navigationBarTitle(Text("First view"))
        }
    }
}
