import SwiftWebUI

struct ContentView: View {
    @State private var selection = 0
    
    var body: some View {
        TabView(selection: $selection) {
            FirstView().tabItem(Text("First")).tag(0)
            SecondView().tabItem(Text("Second")).tag(1)
        }
    }
}

