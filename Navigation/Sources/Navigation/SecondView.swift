import SwiftWebUI

struct SecondView: View {
    var body: some View {
        NavigationView {
            Text("Hello, I am two!")
                .navigationBarTitle(Text("Second view"))
        }
    }
}
